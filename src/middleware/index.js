const authenticateModuleConfigure = require ('./authenticateConfigure.js');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
    authenticateModuleConfigure(app, 'login');
    
  // Add your custom middleware here. Remember that
  // in Express, the order matters.
};
