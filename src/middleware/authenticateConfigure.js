const auth = require('@feathersjs/authentication');
const local = require('@feathersjs/authentication-local');
const jwt = require('@feathersjs/authentication-jwt');
const express = require('@feathersjs/express');    
const memory = require('feathers-memory'); 

module.exports = function (app, authpath) {
  // authpath - route for local authentication service 
  local.options = app.get('authOptions');
  local.options.Verifier=local.Verifier;
  
  app.configure(express.rest());
  app.configure(auth(local.options));
  app.configure(local());
  app.configure(jwt());             
  app.use('/'+authpath, memory());        
      
  app.service(authpath).hooks({
    before: {
      create: [auth.hooks.authenticate(['local', 'jwt'])]
    },
    after: { 
      create: [ local.hooks.protect('password')]
    }  
  });   

  app.service('authentication').hooks({
  before: {
    create: [
    // You can chain multiple strategies
    auth.hooks.authenticate('local', 'jwt')
    ],
    remove: [
    auth.hooks.authenticate('jwt')
    ]
  }
  });
    
}