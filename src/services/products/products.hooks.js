const auth = require('@feathersjs/authentication');

module.exports = {
  before: {
    all:[auth.hooks.authenticate('jwt')]
  },

  after: {
  },

  error: {
  }
};