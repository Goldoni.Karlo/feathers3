import test from 'ava';
import supertest from 'supertest';
const app = require('./_app');
const request = supertest(app);

module.exports = {};

// Variables for testing 
var jwToken='';
var newUserId='';
var newCategoryId='';
var newProductId='';

test.serial('get users', async t => {
  const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body.data;
    for (var i=0; i<users.length; i++)
        {
            console.log (users[i]._id, users[i].email);
            const response2= await request
            .get('/users/'+users[i]._id)
            .set('content-type', 'application/json')
            .expect(200);
            t.is(response2.statusCode, 200)
        }
});

test.serial('create user', async t  => {
    const response = await request
    .post('/users')
    .set('content-type', 'application/json')
    .send({firstname: 'vasya', lastname: 'pupkin', email: 'pupkin.v@gmail.com', password:'12345'})
    .expect(201);
    t.is(response.statusCode, 201)
    // Create copy of user with email pupkin.v@gmail.com must be refused 
    const response2 = await request
    .post('/users')
    .set('content-type', 'application/json')
    .send({firstname: 'vasya', lastname: 'pupkin', email: 'pupkin.v@gmail.com', password:'12345'})
    .expect(409);
    t.is(response2.statusCode, 409)
});


test.serial('patch user', async t => {
    const response= await request
    .get('/users')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const users=response.body.data;
    const user_patch = users.find((element) => element.email === 'pupkin.v@gmail.com');
    // Get newUserId
    newUserId=user_patch._id;
    const response2 = await request
    .patch('/users/'+user_patch._id)
    .set('content-type', 'application/json')
    .send({ firstname: 'VAsya',lastname: 'Pupkin'})
    .expect(200);
    t.is(response2.statusCode, 200)
});


test.serial('put user', async t => {
     const response = await request
     .put('/users/'+newUserId)
     .set('content-type', 'application/json')
     .send({ firstname: 'VASYA', lastname: 'PUP King', email: 'pupkin.v@gmail.com', password: '12345' })
     .expect(200);
     t.is(response.statusCode, 200)
});

test.serial('authenticate user', async t => {
    const response= await request
    .post('/authentication')
    .set('content-type', 'application/json')
    .send({email: 'pupkin.v@gmail.com', password:'12345'})
    .expect(201);
    t.is(response.statusCode, 201)
    // Get token for authentication
    jwToken=response.body.accessToken;
    console.log ('JWT= ',jwToken);
});

test.serial('get categories', async t => {
   const response= await request
     .get('/categories')
     .set('content-type', 'application/json')
     .set('Authorization', jwToken)
     .expect(200);
     t.is(response.statusCode, 200)
 });

 test.serial('create category', async t => {
  const response= await request
    .post('/categories')
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({user: newUserId, name:'Vasya favorite category'})
    .expect(201);
    t.is(response.statusCode, 201)
     // Create copy of category with same name 'Vasya favorite category' must be refused 
    const response2 = await request
     .post('/categories')
     .set('content-type', 'application/json')
     .set('Authorization', jwToken)
     .send({user: newUserId, name:'Vasya favorite category'})
     .expect(409);
    t.is(response2.statusCode, 409)
});

test.serial('patch category', async t => {
  const response= await request
    .get('/categories')
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .expect(200);
    t.is(response.statusCode, 200)
    const categories=response.body.data;
    const category_patch = categories.find((element) => element.user === newUserId);
    // Get newUserId
    newCategoryId=category_patch._id;
  const response2= await request
    .patch('/categories/'+newCategoryId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({name:'Vasya favorite alcohol'})
    .expect(200);
    t.is(response.statusCode, 200)
});

test.serial('put category', async t => {
  const response = await request
    .put('/categories/'+newCategoryId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({user: newUserId, name:'Vasya best favorite alcohol'})
    .expect(200); 
  t.is(response.statusCode, 200)    
});

test.serial('get products', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .expect(200);
    t.is(response.statusCode, 200)
});

test.serial('create product', async t => {
  const response= await request
    .post('/products')
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({user: newUserId, category: newCategoryId ,name:'Vasino favorite Pivo'})
    .expect(201);
    t.is(response.statusCode, 201)
     // Create copy of product with same name 'Pivo' must be refused 
    const response2 = await request
     .post('/products')
     .set('content-type', 'application/json')
     .set('Authorization', jwToken)
     .send({user: newUserId, category: newCategoryId ,name:'Vasino favorite Pivo'})
     .expect(409);
    t.is(response2.statusCode, 409)
});

test.serial('patch product', async t => {
  const response= await request
    .get('/products')
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .expect(200);
    t.is(response.statusCode, 200)
  const products=response.body.data;
  const product_patch = products.find((element) => element.user === newUserId);
  // Get newUserId
  newProductId=product_patch._id;
  const response2= await request
    .patch('/products/'+newProductId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({name:'Vasino best favorite Pivo'})
    .expect(200);
    t.is(response.statusCode, 200)
});

test.serial('put product', async t => {
  const response = await request
    .put('/products/'+newProductId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .send({user: newUserId, name:'Vasino best favorite Pivo is Vodka', category: newCategoryId})
    .expect(200); 
  t.is(response.statusCode, 200)    
});

test.serial('delete product', async t => {
  const response = await request
    .delete('/products/'+newProductId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .expect(200); 
  t.is(response.statusCode, 200)    
});

test.serial('delete category', async t => {
  const response = await request
    .delete('/categories/'+newCategoryId)
    .set('content-type', 'application/json')
    .set('Authorization', jwToken)
    .expect(200); 
  t.is(response.statusCode, 200)    
});

test.serial('delete user', async t => {
   const response = await request
     .delete('/users/'+newUserId)
     .set('content-type', 'application/json')
     .expect(200); 
   t.is(response.statusCode, 200)    
});